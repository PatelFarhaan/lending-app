from flask import Flask, Blueprint, render_template

error_handler_blueprint = Blueprint('error_handler',
                                    __name__,
                                    static_folder='static',
                                    template_folder='templates')

@error_handler_blueprint.app_errorhandler(404)
def error_404(e):
    return render_template('error/404.html'), 404


@error_handler_blueprint.app_errorhandler(403)
def error_403(e):
    return render_template('error/403.html'), 403
