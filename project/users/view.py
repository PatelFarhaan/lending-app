import datetime
from project import serial, razorpay_client
from project.users.interest import interestLogic
from project.users.models import Users, Transactions
from project.users.disposal_emails import is_disposable
from project.users.message import clientOtpSend, clienMobileVerification
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_required, login_user, logout_user, current_user
from project.users.mails import passwordResetMail, passwordResetConfirmationMail, emailConfirmationMail
from flask import render_template, request, Blueprint, url_for, session, redirect, flash, get_flashed_messages, abort

## create the session for flash and then check for the bootstrap alerts

users_blueprint = Blueprint('users',
                            __name__,
                            static_folder='static',
                            template_folder='templates')


@users_blueprint.route('/')
def homepage():
    session.clear()
    return render_template('homepage.html')


@users_blueprint.route('/login', methods=['GET','POST'])
def login():
    session.clear()
    if request.method == 'POST':
        mobile_number = request.form.get('mobileNumber', None)
        password = request.form.get('loginPassowrd', None)
        client = Users.objects.filter(mobileNumber=mobile_number).first()

        if client:
            if check_password_hash(client.hashedPassword, password):
                if client.emailConfirmed == False:
                    token = serial.dumps(client.email, salt='email-confirm')
                    link = url_for('users.confirmEmail', token=token, _external=True)
                    email_response = emailConfirmationMail(_name=client.firstName,
                                                           _confirm_link=link,
                                                           _to=client.email)
                    return render_template('index.html',
                                           message='Please confirm your email address to Login. A link has been sent to your email.')

                if client.mobileNumberVerified == False:
                    session['email'] = client.email
                    token = serial.dumps(session['email'], salt='email-confirm')
                    link = url_for('users.confirmEmail', token=token, _external=True)
                    generated_otp = clienMobileVerification(mobile_number)
                    otp_obj = {'otp': generated_otp, 'datetime': datetime.datetime.utcnow()}
                    client.clientOtp = otp_obj
                    client.save()
                    return redirect(url_for('users.verifyMobileNumber'))

                else:
                    login_user(client)
                    return redirect(url_for('users.accountPage'))
            else:
                return render_template('index.html', message='Incorrect Mobile Number or Password')
        else:
            return render_template('index.html', message='Mobile number does not exist')
    else:
        return render_template('index.html')


@users_blueprint.route('/account-page')
@login_required
def accountPage():
    first_name = current_user.firstName
    users_transaction_obj = list(Transactions.objects.filter(clientId='8788499518@boltpay').all())
    return render_template('myaccount.html', first_name=first_name, users_data=users_transaction_obj[:3])


@users_blueprint.route('/transactions-summary')
@login_required
def summary():
    first_name = current_user.firstName
    users_transaction_obj = list(Transactions.objects.filter(clientId='8788499518@boltpay').all())
    return render_template('summary.html', first_name=first_name, users_data=users_transaction_obj)


@users_blueprint.route('/sendmoney', methods=['GET', 'POST'])
@login_required
def sendmoney():
    if request.method == 'POST':
        reciever_amount = float(request.form.get('amount')) * 100
        reciever_name = request.form.get('name', None)
        reciever_email = request.form.get('active_full_name', None)
        reciever_phone_number = request.form.get('active_phone_number', None)
        data = {
            "customer": {
                "name": current_user.firstName,
                "email": current_user.email,
                "contact": current_user.mobileNumber
            },
            "type": "link",
            "amount": reciever_amount,
            "currency": "INR",
            "description": reciever_name
        }
        response = razorpay_client.invoice.create(data=data)
        transaction_obj = Transactions(
            status='Lent money',
            hasLendedMoney=True,
            amount=reciever_amount,
            hasAcceptedMoney=False,
            email=current_user.email,
            recieverName=reciever_name,
            recieverEmail=reciever_email,
            clientId=current_user.clientId,
            invoiceCreationId=response['id'],
            razorInvoiceCreationResponse=response,
            mobileNumber=current_user.mobileNumber,
            recieverMobileNumber=reciever_phone_number,
        )
        transaction_obj.save()
        link = response['short_url']
        return redirect(link)

    first_name = current_user.firstName
    return render_template('sendmoney.html', first_name=first_name)


@users_blueprint.route('/webhook', methods=['GET', 'POST'])
def webhook():
    if request.method == 'POST':
        if request.json['event'] == 'invoice.paid':
            jsondata = request.json
            inv_id = jsondata['payload']['invoice']['entity']['id']
            db_inv_id = Transactions.objects(invoiceCreationId=inv_id).first()
            db_inv_id.razorInvoicePaymentResponse = jsondata
            db_inv_id.save()
            return "None"
        else:
            return "None"


@users_blueprint.route('/requestmoney')
@login_required
def requestmoney():
    first_name = current_user.firstName
    return render_template('sendmoney.html', first_name=first_name)


@users_blueprint.route('/signup', methods=['GET', 'POST'])
def signup():
    session.clear()
    if request.method == 'POST':
        session['email'] = request.form.get('userEmail', None)
        session['password'] = request.form.get('userPW', None)
        session['confirm_password'] = request.form.get('userConfirmPW', None)

        if session['email']:
            users_obj = Users.objects.filter(email=session['email']).first()
            if users_obj:
                session.clear()
                return render_template('sign-up.html', message='Email Already Exists')
            elif not session['password'] or not session['confirm_password'] or \
                    (session['password'] != session['confirm_password']):
                session.clear()
                return render_template('sign-up.html', message='Passwords does not match.')
            elif is_disposable(session['email'].split('@')[1]):
                return render_template('sign-up.html', message='Enter authentic email address.')
            else:
                return redirect(url_for('users.new_account'))
        else:
            return redirect(url_for('users.new_account'))
    else:
        return render_template('sign-up.html')


@users_blueprint.route('/new-account', methods=['GET', 'POST'])
def new_account():
    if request.method == 'POST':
        city = request.form.get('userCityName', None)
        last_name = request.form.get('lastName', None)
        state = request.form.get('userStateName', None)
        first_name = request.form.get('firstName', None)
        form_check = request.form.get('checkBoxTnC', None)
        mobile_number = request.form.get('mobileNumber', None)
        existing_number = Users.objects.filter(mobileNumber=mobile_number).first()

        if not form_check:
            return render_template('newAccount.html', message='Please click the check box to agree the terms and conditions')

        if not existing_number:

            new_user_obj = Users(hashedPassword=generate_password_hash(session['password']),
                                 clientId=f'{str(mobile_number)}@boltpay',
                                 mobileNumber=mobile_number,
                                 email=session['email'],
                                 emailConfirmed=False,
                                 isClientActive=False,
                                 firstName=first_name,
                                 isClientAdmin=False,
                                 lastName=last_name,
                                 state=state,
                                 city=city)

            ## should only be there for 5 mins

            token = serial.dumps(session['email'], salt='email-confirm')
            link = url_for('users.confirmEmail', token=token, _external=True)

            emailConfirmationMail(_name=first_name,
                                  _confirm_link=link,
                                  _to=session['email'])
            generated_otp = clienMobileVerification(mobile_number)
            otp_obj = {'otp': generated_otp, 'datetime': datetime.datetime.utcnow()}
            new_user_obj.clientOtp = otp_obj
            new_user_obj.save()
            email = session['email']
            session.clear()
            session['email'] = email
            return redirect(url_for('users.verifyMobileNumber'))
        else:
            return render_template('index.html', message='Mobile number already exists')
    else:
        if 'email' not in session or 'password' not in session or \
                'confirm_password' not in session:
            session.clear()
            return redirect(url_for('users.signup'))
        return render_template('newAccount.html')


@users_blueprint.route('/email-confirm/<token>', methods=['GET'])
def confirmEmail(token):
    try:
        email = serial.loads(token, salt='email-confirm', max_age=500)
        user_obj = Users.objects.filter(email=email).first_or_404()
        user_obj.emailConfirmed = True
        user_obj.save()
        s_message = 'Email has been saved successfully. Please login to continue'
        return redirect(url_for('users.login'))
    except:
        return render_template('expired-password.html')


@users_blueprint.route('/verify-mobile-number', methods=['GET', 'POST'])
def verifyMobileNumber():
    if request.method == 'POST':
        recieved_otp = request.form.get('aws-sns-otp', None)
        otp_resend = request.form.get('resend-otp', None)

        if otp_resend:
            email = session.get('email', None)
            users_obj = Users.objects.filter(email=email).first_or_404()

            generated_otp = clientOtpSend(users_obj.mobileNumber)
            otp_obj = {'otp': generated_otp, 'datetime': datetime.datetime.utcnow()}
            ## set the time for the otp
            users_obj.clientOtp = otp_obj
            users_obj.save()
            session['email'] = users_obj.email
            return render_template('verify-mobile-number.html', s_message='OTP send successfully.')

        if recieved_otp:
            email = session.get('email', None)
            users_obj = Users.objects.filter(email=email).first_or_404()
            otp = users_obj.clientOtp['otp']

            isOtpExpired = int((datetime.datetime.utcnow() - users_obj.clientOtp['datetime']).total_seconds())
            if isOtpExpired <= 300:
                if int(otp) == int(recieved_otp):
                    users_obj.clientOtp = None
                    users_obj.mobileNumberVerified = True
                    users_obj.save()
                    session.clear()
                    ## flash mobile number has been successfully saved. please login to continue
                    return redirect(url_for('users.login'))
                else:
                    return render_template('verify-mobile-number.html', message='Wrong OTP')
            else:
                ## OTP expired
                render_template('verify-mobile-number.html', message='OTP Expired')
        else:
            render_template('verify-mobile-number.html', message='Wrong OTP')
    else:
        if 'email' in session:
            return render_template('verify-mobile-number.html')
        else:
            session.clear()
            abort(403)


@users_blueprint.route('/email-password-reset', methods=['GET', 'POST'])
def emailPasswordReset():
    if request.method == 'POST':
        email = request.form.get('reset-email-address', None)

        if email:
            user_obj = Users.objects.filter(email=email).first()
            if user_obj:
                token = serial.dumps(user_obj.email, salt='email-reset')
                link = url_for('users.resetLink', token=token, _external=True)
                mail_response = passwordResetMail(_reset_link=link,
                                                  _to=user_obj.email,
                                                  _name=user_obj.firstName)
                if mail_response:
                    return render_template('email-password-reset.html', message='Link has been sent to your email.')
                else:
                    return render_template('email-password-reset.html', message='Something went wrong. Try Again.')
            else:
                return render_template('email-password-reset.html', error_message='That Email Address does not Exist')
        else:
            return redirect(url_for('users.emailPasswordReset'))

    return render_template('email-password-reset.html')


@users_blueprint.route('/reset-link/<token>', methods=['GET', 'POST'])
def resetLink(token):
    try:
        email = serial.loads(token, salt='email-reset', max_age=500)
        user_obj = Users.objects.filter(email=email).first_or_404()

        if request.method == 'POST':
            if user_obj:
                password = request.form.get('new-client-password', None)
                confirm_password = request.form.get('confirm-new-client-password', None)

                if password == confirm_password:
                    if check_password_hash(user_obj.hashedPassword, password):
                        return render_template('new-password.html', error_message='New Password cannot be same as previous password')
                    else:
                        user_obj.hashedPassword = generate_password_hash(password)
                        user_obj.save()
                        passwordResetConfirmationMail(user_obj.email, user_obj.firstName)

                        session.clear()
                        message='Password saved successfully. Please login to continue.'
                        return redirect(url_for('users.login'))
                else:
                    return render_template('new-password.html', error_message='Both passwords should be same.')
        else:
            return render_template('new-password.html')
    except:
        return render_template('expired-password.html')


@users_blueprint.route('/otp-password-reset', methods=['GET', 'POST'])
def otpPasswordReset():
    if request.method == 'POST':
        mobile_number = request.form.get('mobileNumber', None)
        if mobile_number:
            users_obj = Users.objects.filter(mobileNumber=mobile_number).first()

            if users_obj:
                generated_otp = clientOtpSend(users_obj.mobileNumber)
                otp_obj = {'otp': generated_otp, 'datetime': datetime.datetime.utcnow()}
                ## set the time for the otp
                users_obj.clientOtp = otp_obj
                users_obj.save()
                session['current_user_email'] = users_obj.email
                return redirect(url_for('users.resetOtp'))
            else:
                return render_template('otp-password-reset.html', message='Mobile number is not registered')
        else:
            return redirect(url_for('users.otpPasswordReset'))
    else:
        return render_template('otp-password-reset.html')


@users_blueprint.route('/resetOtp', methods=['GET', 'POST'])
def resetOtp():
    if request.method == 'POST':
        # check datetime and clear otp in db first here and then do the rest
        otp = request.form.get('aws-sns-otp', None)

        if otp:
            if 'current_user_email' in session:
                users_obj = Users.objects.filter(email=session['current_user_email']).first()

                isOtpExpired = int((datetime.datetime.utcnow() - users_obj.clientOtp['datetime']).total_seconds())
                if isOtpExpired <= 300:
                    if int(otp) == int(users_obj.clientOtp['otp']):
                        return redirect(url_for('users.resetOtpPasswordPage'))
                    else:
                        return render_template('new-otp.html', message='Wrong OTP')
                else:
                    ## otp expired
                    return redirect(url_for('users.otpPasswordReset'))
            else:
                return redirect(url_for('users.otpPasswordReset'))
        else:
            return redirect(url_for('users.resetOtp'))
    else:
        return render_template('new-otp.html')


@users_blueprint.route('/reset-otp-password-page', methods=['GET', 'POST'])
def resetOtpPasswordPage():
    if request.method == 'POST':
        new_password = request.form.get('new-client-password', None)
        confirm_password = request.form.get('confirm-new-client-password', None)
        user_obj = Users.objects.filter(email=session['current_user_email']).first_or_404()

        if new_password == confirm_password:
            if check_password_hash(user_obj.hashedPassword, new_password):
                return render_template('reset-otp-password-page.html', error_message='New Password cannot be same as previous password')
            else:
                user_obj.hashedPassword = generate_password_hash(new_password)
                user_obj.clientOtp = None
                user_obj.save()
                passwordResetConfirmationMail(user_obj.email, user_obj.firstName)
                session.clear()
                flash('Password changed successfully.')
                return redirect(url_for('users.login'))
        else:
            return render_template('new-password.html', error_message='Both passwords should be same.')
    else:
        current_user_email = session.get('current_user_email', None)
        if current_user_email:
            return render_template('reset-otp-password-page.html')
        else:
            return redirect(url_for('users.resetOtp'))


@users_blueprint.route('/logout', methods=['GET','POST'])
@login_required
def logout():
    logout_user()
    session.clear()
    print("user logged out!!!")
    return redirect(url_for('users.login'))


@users_blueprint.route('/interview-with-farhaan')
def farhaan():
    return render_template('personal/interview.html')

payment_response = {'entity': 'event', 'account_id': 'acc_CqRBbYkdWjfebd', 'event': 'invoice.paid', 'contains': ['payment', 'order', 'invoice'], 'payload': {'payment': {'entity': {'id': 'pay_D5oV16GXOvhvcj', 'entity': 'payment', 'amount': 2000, 'currency': 'INR', 'status': 'captured', 'order_id': 'order_D5oUubLXsqd9j6', 'invoice_id': 'inv_D5oUuaiHmjYmYS', 'international': False, 'method': 'upi', 'amount_refunded': 0, 'refund_status': None, 'captured': True, 'description': '#inv_D5oUuaiHmjYmYS', 'card_id': None, 'bank': None, 'wallet': None, 'vpa': 'patelfarhaan@okaxis', 'email': 'patel.farhaaan@gmail.com', 'contact': '+918788499518', 'notes': [], 'fee': 48, 'tax': 8, 'error_code': None, 'error_description': None, 'created_at': 1565786068}}, 'order': {'entity': {'id': 'order_D5oUubLXsqd9j6', 'entity': 'order', 'amount': 2000, 'amount_paid': 2000, 'amount_due': 0, 'currency': 'INR', 'receipt': None, 'offer_id': None, 'offers': {'entity': 'collection', 'count': 0, 'items': []}, 'status': 'paid', 'attempts': 1, 'notes': [], 'created_at': 1565786062}}, 'invoice': {'entity': {'id': 'inv_D5oUuaiHmjYmYS', 'entity': 'invoice', 'receipt': None, 'invoice_number': None, 'customer_id': 'cust_D4kUO4O0W6BqVI', 'customer_details': {'id': 'cust_D4kUO4O0W6BqVI', 'name': 'Farhaan', 'email': 'patel.farhaaan@gmail.com', 'contact': '8788499518', 'gstin': None, 'billing_address': None, 'shipping_address': None, 'customer_name': 'Farhaan', 'customer_email': 'patel.farhaaan@gmail.com', 'customer_contact': '8788499518'}, 'order_id': 'order_D5oUubLXsqd9j6', 'payment_id': 'pay_D5oV16GXOvhvcj', 'status': 'paid', 'expire_by': None, 'issued_at': 1565786062, 'paid_at': 1565786068, 'cancelled_at': None, 'expired_at': None, 'sms_status': 'sent', 'email_status': 'sent', 'date': 1565786062, 'terms': None, 'partial_payment': False, 'gross_amount': 2000, 'tax_amount': 0, 'taxable_amount': 0, 'amount': 2000, 'amount_paid': 2000, 'amount_due': 0, 'first_payment_min_amount': None, 'currency': 'INR', 'currency_symbol': '₹', 'description': 'Farhaan', 'notes': [], 'comment': None, 'short_url': 'https://rzp.io/i/E6M7y6X', 'view_less': True, 'billing_start': None, 'billing_end': None, 'type': 'link', 'group_taxes_discounts': False, 'supply_state_code': None, 'user_id': None, 'created_at': 1565786062, 'idempotency_key': None}}}, 'created_at': 1565786068}
creation_response = {'id': 'inv_D5oUuaiHmjYmYS', 'entity': 'invoice', 'receipt': None, 'invoice_number': None, 'customer_id': 'cust_D4kUO4O0W6BqVI', 'customer_details': {'id': 'cust_D4kUO4O0W6BqVI', 'name': 'Farhaan', 'email': 'patel.farhaaan@gmail.com', 'contact': '8788499518', 'gstin': None, 'billing_address': None, 'shipping_address': None, 'customer_name': 'Farhaan', 'customer_email': 'patel.farhaaan@gmail.com', 'customer_contact': '8788499518'}, 'order_id': 'order_D5oUubLXsqd9j6', 'line_items': [], 'payment_id': None, 'status': 'issued', 'expire_by': None, 'issued_at': 1565786062, 'paid_at': None, 'cancelled_at': None, 'expired_at': None, 'sms_status': 'pending', 'email_status': 'pending', 'date': 1565786062, 'terms': None, 'partial_payment': False, 'gross_amount': 2000, 'tax_amount': 0, 'taxable_amount': 0, 'amount': 2000, 'amount_paid': 0, 'amount_due': 2000, 'currency': 'INR', 'currency_symbol': '₹', 'description': 'Farhaan', 'notes': [], 'comment': None, 'short_url': 'https://rzp.io/i/E6M7y6X', 'view_less': True, 'billing_start': None, 'billing_end': None, 'type': 'link', 'group_taxes_discounts': False, 'created_at': 1565786062}

# Allow users transaction only if usere is on boltpay
# Send email to user when
# do razaorpay webhook verification