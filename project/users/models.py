import datetime
from flask_login import UserMixin
from project import db, login_manager


@login_manager.user_loader
def user_load(user_id):
    return Users.objects.get(pk=user_id)


class Users(db.Document, UserMixin):
    clientOtp = db.DictField()
    city = db.StringField(required=True)
    state = db.StringField(required=True)
    hashedPassword = db.StringField(required=True)
    emailConfirmed = db.BooleanField(default=False)
    isClientActive = db.BooleanField(default=False)
    email = db.EmailField(required=True,unique=True)
    clientId = db.StringField(required=True, unique=True)
    mobileNumberVerified = db.BooleanField(default=False)
    lastName = db.StringField(required=True, max_length=50)
    firstName = db.StringField(required=True, max_length=50)
    mobileNumber = db.StringField(required=True,unique=True)
    isClientAdmin = db.BooleanField(required=True, default=False)
    createdOn = db.DateTimeField(default=datetime.datetime.utcnow())

    meta = {
        'indexes': ['clientId', 'email', 'mobileNumber', 'isClientAdmin',
                    'isClientActive', '-createdOn']
    }


class Transactions(db.Document, UserMixin):
    email = db.EmailField(required=True)
    amount = db.FloatField(required=True)
    status = db.StringField(required=True)
    clientId = db.StringField(required=True)
    recieverEmail = db.EmailField(required=True)
    mobileNumber = db.StringField(required=True)
    recieverName = db.StringField(required=True)
    hasLendedMoney = db.BooleanField(default=False)
    invoiceCreationId = db.StringField(required=True)
    hasAcceptedMoney = db.BooleanField(default=False)
    recieverMobileNumber = db.StringField(required=True)
    razorInvoicePaymentResponse = db.DictField(sparse=True)
    razorInvoiceCreationResponse = db.DictField(required=True)
    currentTimestamp = db.DateTimeField(default=datetime.datetime.utcnow())
    date = db.StringField(default=datetime.datetime.strftime(datetime.datetime.utcnow(), '%d %b %Y'))
    meta = {
        'indexes': ['email', 'mobileNumber', '-date', 'invoiceCreationId']
    }


class BankCards(db.Document, UserMixin):
    email = db.EmailField(required=True)
    clientId = db.StringField(required=True)
    bankName = db.StringField(required=True)
    mobileNumber = db.StringField(required=True)
    bankAccountNumber = db.StringField(required=True)
    currentTimestamp = db.DateTimeField(default=datetime.datetime.utcnow())

    meta = {
        'indexes': ['email', 'clientId', 'mobileNumber']
    }

# write a script which would execute everyday to check lending money payments and create a razotpay
# virtual account for the same.