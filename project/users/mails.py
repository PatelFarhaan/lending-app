import boto3


def passwordResetMail(_to, _name, _reset_link):
    SUBJECT = "PASSWORD RESET LINK"
    BODY_TEXT = (f'''
            Dear {_name},

                    Click the link below to reset your password:
                    {_reset_link}

                Thanks and Regards,
                Bolt Pay.
        ''')

    try:
        client = boto3.client('ses',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAQFHUN5WXQSBQ72WS',
                              aws_secret_access_key='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',
                              )

        response = client.send_email(
            Destination={
                'ToAddresses': [
                    _to,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': 'UTF-8',
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': 'UTF-8',
                    'Data': SUBJECT,
                },
            },
            Source='noreply@boltpay.in',
        )
        print("response is: ", response)
        return True
    except:
        return False


def passwordResetConfirmationMail(_to, _name):
    SUBJECT = "PASSWORD CHANGED SUCCESSFULLY"
    BODY_TEXT = (f'''
            Dear {_name},

                    Your Password has been saved successfully.
                    Please login with you new password.

                Thanks and Regards,
                Bolt Pay.
        ''')

    try:
        client = boto3.client('ses',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAQFHUN5WXQSBQ72WS',
                              aws_secret_access_key='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',
                              )

        response = client.send_email(
            Destination={
                'ToAddresses': [
                    _to,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': 'UTF-8',
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': 'UTF-8',
                    'Data': SUBJECT,
                },
            },
            Source='noreply@boltpay.in',
        )
        print("response is: ", response)
        return True
    except:
        return False


def emailConfirmationMail(_to, _name, _confirm_link):
    SUBJECT = "VERIFY YOUR EMAIL ADDRESS"
    BODY_TEXT = (f'''
            Dear {_name},

                    Please click the link below to confirm your email address:
                    {_confirm_link}

                Thanks and Regards,
                Bolt Pay.
        ''')

    try:
        client = boto3.client('ses',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAQFHUN5WXQSBQ72WS',
                              aws_secret_access_key='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',
                              )

        response = client.send_email(
            Destination={
                'ToAddresses': [
                    _to,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': 'UTF-8',
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': 'UTF-8',
                    'Data': SUBJECT,
                },
            },
            Source='noreply@boltpay.in',
        )
        print("response is: ", response)
        return True
    except:
        return False
    
    
def paymentSuccessfullySent(_to, _name, amount, reciever_name):
    SUBJECT = "Transaction Successful"
    BODY_TEXT = (f'''
            Dear {_name},

                Payment of Rs.{amount} is successfully transferred to {reciever_name}.

                Thanks and Regards,
                Bolt Pay.
        ''')

    try:
        client = boto3.client('ses',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAQFHUN5WXQSBQ72WS',
                              aws_secret_access_key='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',
                              )

        response = client.send_email(
            Destination={
                'ToAddresses': [
                    _to,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': 'UTF-8',
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': 'UTF-8',
                    'Data': SUBJECT,
                },
            },
            Source='noreply@boltpay.in',
        )
        print("response is: ", response)
        return True
    except:
        return False
    

def paymentSuccessfullyRecieved(_to, _name, _reset_link, amount, sender_name):
    SUBJECT = "Transaction Successful"
    BODY_TEXT = (f'''
                Dear {_name},

                    Payment of Rs.{amount} is successfully recieved by {sender_name}.

                    Thanks and Regards,
                    Bolt Pay.
            ''')

    try:
        client = boto3.client('ses',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAQFHUN5WXQSBQ72WS',
                              aws_secret_access_key='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',
                              )

        response = client.send_email(
            Destination={
                'ToAddresses': [
                    _to,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': 'UTF-8',
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': 'UTF-8',
                    'Data': SUBJECT,
                },
            },
            Source='noreply@boltpay.in',
        )
        print("response is: ", response)
        return True
    except:
        return False