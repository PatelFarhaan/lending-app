
def interestLogic(amount):
    amount = float(amount)
    razor_pay_interest = ((2/100)*amount)
    razor_pay_gst_interest = ((0.36/100)*amount)
    boltpay_interest = ((1/amount)*100)
    amount += razor_pay_interest + razor_pay_gst_interest +boltpay_interest
    amount = round(float(amount), len(str(amount).split('.')[1]))
    return float(amount)