import os
import sys
import razorpay
from flask import Flask
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from itsdangerous import URLSafeTimedSerializer

sys.path.append('../')
app = Flask(__name__,
            static_folder='static',
            template_folder='templates')

app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', 'jagsvd@#$@#D')
razorpay_client = razorpay.Client(auth=("rzp_test_niFDHt200sAFDr", "PDgc2TtSmZpC11qGjVivK2Uo"))
app.config['MONGODB_SETTINGS'] = {'host': os.environ.get('MONGODB_SETTINGS', 'mongodb://prodDb:speculative$123k@13.235.167.195:27017/admin')}
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI', 'mongodb://prodDb:speculative$123k@13.235.167.195:27017/admin')
serial = URLSafeTimedSerializer(os.environ.get('SECRET_KEY', ''))


db = MongoEngine(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'users.login'


from project.users.view import users_blueprint
from project.error.error_handler import error_handler_blueprint

app.register_blueprint(users_blueprint)
app.register_blueprint(error_handler_blueprint)